//We can also have an array of objects. We can group together objects in an array. That array can also use array methods for the objects. However, being objects are more complex than strings or numbers, there is a difference when handling them.

let users = [

	{

		name: "Mike Shell",
		username: "mikeBlueShell",
		email: "mikeyShell01@gmail.com",
		password: "iammikey1999",
		isActive: true,
		dateJoined: "August 8,2011"

	},
	{

		name: "Jake Janella",
		username: "jjnella99",
		email: "jakejanella_99@gmail.com",
		password: "jakiejake12",
		isActive: true,
		dateJoined: "January 15, 2015"

	}

];

//Much like a regular array, we can actually access this array of objects the same way.

console.log(users[0]);

//How can we access the properties of our objects in an array?

//Show the second items' email property:
console.log(users[1].email);

//Can we update the properties of objects in an array?

users[0].email = "mikeKingOfShells@gmail.com";
users[1].email = "janellaJakeArchitect@gmail.com";
console.log(users[0].email);
console.log(users[1].email);

//Can we also use array methods for array of objects?

//Add another user into the array:

users.push({

	name: "James Jameson",
	username: "iHateSpidey",
	email: "jamesJjameson@gmail.com",
	password: "spideyisamenace64",
	isActive: true,
	dateJoined: "February 14, 2000"

})

class User{
	constructor(name,username,email,password){

		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
		this.isActive = true;
		this.dateJoined = "September 28, 2021"
	}
}

let newUser1 = new User("Kate Midlletown","notTheDuchess","seriouslyNotDuchess@gmail.com","notRoyaltyAtaLL");

console.log(newUser1);

users.push(newUser1);

console.log(users);

//find() - is able to return the found item.

function login(username,password){

	//check if the username does exist or any user uses that username
	//check if the password given matches the password of our user in the array.
	let userFound = users.find((user) => {

		//user is a parameter which receives each items in the users array and the users array is an array of objects. Therefore, we can expect that the user parameter will contain an object.

		return user.username === username && user.password === password

	})
	console.log(userFound);
	if(userFound){
		alert(`Thank you for logging in, ${userFound.username}`)
	} else { 
		alert("Login Failed. Invalid Credentials.")
	}
	

}

/*login("notTheDuchess","notRoyaltyAtaLL");
login("iHateSpidey","spideyisamenace64");
login("acdp","aaaa");*/

let courses = [];

class postCourse {
	constructor(id,name,description,price){

		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.isActive = true;

	}
}


const addNewCourse = (id,name,description,price) => {
	let newCourse1 = new postCourse(id,
		name,description,price);
	courses.push(newCourse1);
	alert(`You have created ${newCourse1.name}. The price is ${newCourse1.price}.`)
}

addNewCourse("001","The Python Mega Course","Learn Python from zero to hero in 1 week",1500);



const getSingleCourse = (id) => {

	let coursesFound = courses.find((course) => id === course.id); 

	return (coursesFound);
}

getSingleCourse("001");

const deleteCourse = () => courses.pop();


















































